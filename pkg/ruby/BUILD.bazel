load("@aspect_bazel_lib//lib:write_source_files.bzl", "write_source_files")
load("@rules_proto_grpc//ruby:defs.bzl", "ruby_proto_compile")
load("//build:proto_def.bzl", "ruby_grpc_compile")

PROTO = [
    "modshared",
    "modserver",
    "agent_tracker",
]

RPC = [
    "agent_tracker",
    "configuration_project",
    "notifications",
]

[
    ruby_proto_compile(
        name = "%s_proto" % name,
        protos = [
            "//internal/module/%s:proto" % name,
        ],
        tags = ["manual"],
    )
    for name in PROTO
]

[
    ruby_proto_compile(
        name = "%s_rpc_proto" % name,
        protos = [
            "//internal/module/%s/rpc:proto" % name,
        ],
        tags = ["manual"],
    )
    for name in RPC
]

[
    ruby_grpc_compile(
        name = "%s_services_proto" % name,
        protos = [
            "//internal/module/%s/rpc:proto" % name,
        ],
        tags = ["manual"],
    )
    for name in RPC
]

write_source_files(
    name = "extract_generated",
    diff_test = False,
    files = {
        "lib/internal/module/%s/%s_pb.rb" % (name, name): ":%s_proto" % name
        for name in PROTO
    } | {
        "lib/internal/module/%s/rpc/rpc_pb.rb" % name: ":%s_rpc_proto" % name
        for name in RPC
    } | {
        "lib/internal/module/%s/rpc/rpc_services_pb.rb" % name: ":%s_services_proto" % name
        for name in RPC
    },
    tags = ["manual"],
    visibility = ["//visibility:public"],
)
